package com.example.jobsearchapp.listeners;

import com.example.jobsearchapp.models.ResultModel;

public interface OnResultClicked {

    public void openPosition(ResultModel selectedResult);
}
