package com.example.jobsearchapp.listeners;

import android.view.View;

public interface OnSkillsListener {

    void onSkillsClicked(String text);
}
