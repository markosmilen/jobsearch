package com.example.jobsearchapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.jobsearchapp.R;
import com.example.jobsearchapp.listeners.OnSkillsListener;
import com.example.jobsearchapp.models.SkillsModel;

import java.util.ArrayList;

public class SkillAdapter extends RecyclerView.Adapter<SkillAdapter.SkillsViewHolder>{

   OnSkillsListener listener;
   LayoutInflater inflater;
   ArrayList<SkillsModel> skills;

    public SkillAdapter(Context context, ArrayList<SkillsModel> skills, OnSkillsListener listener){
        this.inflater = LayoutInflater.from(context);
        this.skills = skills;
        this.listener = listener;
    }

    @NonNull
    @Override
    public SkillsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_skills, parent,false);
        return new SkillsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SkillsViewHolder holder, int position) {
        SkillsModel oneSkill = skills.get(position);
        holder.skillName.setText(oneSkill.getSkill());
    }

    @Override
    public int getItemCount() {
        return skills.size();
    }

    public SkillsModel getSkill(int position){
        return skills.get(position);
    }


    public class SkillsViewHolder extends RecyclerView.ViewHolder{

        TextView skillName;

        public SkillsViewHolder(@NonNull View itemView) {
            super(itemView);
            skillName = (TextView) itemView.findViewById(R.id.skills_item_textview_id);
            skillName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null){
                        listener.onSkillsClicked(skillName.getText().toString());
                    }
                }
            });
        }
    }
}
