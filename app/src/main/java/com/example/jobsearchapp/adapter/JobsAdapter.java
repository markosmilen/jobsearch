package com.example.jobsearchapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.jobsearchapp.R;
import com.example.jobsearchapp.listeners.OnResultClicked;
import com.example.jobsearchapp.models.ResultModel;

import java.util.List;

public class JobsAdapter extends RecyclerView.Adapter<JobsAdapter.JobsViewHolder> {

    OnResultClicked resultsListener;
    LayoutInflater inflater;
    Context context;
    List<ResultModel> results;

    public JobsAdapter(Context context, List<ResultModel> results, OnResultClicked resultsListener) {
        this.inflater = LayoutInflater.from(context) ;
        this.context = context;
        this.results = results;
        this.resultsListener = resultsListener;
    }

    @NonNull
    @Override
    public JobsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_result, parent, false);
        return new JobsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull JobsViewHolder holder, int position) {
        ResultModel result = results.get(position);
        String url = result.getCompany_logo();
        Glide.with(context).load(url).placeholder(R.drawable.kitty).into(holder.logo);
        holder.jobLocaton.setText(result.getLocation());
        holder.jobCompany.setText(result.getCompany());
        holder.jobTitle.setText(result.getTitle());
    }

    @Override
    public int getItemCount() {
        return results.size();
    }

    public void getPosition (int position){
        results.get(position);
    }

    public class JobsViewHolder extends RecyclerView.ViewHolder {
        ImageView logo;
        TextView jobTitle, jobCompany, jobLocaton;

        public JobsViewHolder(@NonNull View itemView) {
            super(itemView);
            logo = (ImageView) itemView.findViewById(R.id.item_result_logo_id);
            jobTitle = (TextView) itemView.findViewById(R.id.item_result_title_id);
            jobCompany = (TextView) itemView.findViewById(R.id.item_result_company_id);
            jobLocaton = (TextView) itemView.findViewById(R.id.item_result_location_id);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(resultsListener != null){
                        resultsListener.openPosition(results.get(getAdapterPosition()));
                    }
                }
            });
        }
    }
}
