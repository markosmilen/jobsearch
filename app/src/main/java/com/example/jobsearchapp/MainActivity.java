package com.example.jobsearchapp;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.jobsearchapp.adapter.JobsAdapter;
import com.example.jobsearchapp.adapter.SkillAdapter;
import com.example.jobsearchapp.listeners.OnResultClicked;
import com.example.jobsearchapp.listeners.OnSkillsListener;
import com.example.jobsearchapp.models.ResultModel;
import com.example.jobsearchapp.models.SkillsModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity implements OnSkillsListener, OnResultClicked {
    public static final String ALL = "search=node";
    public static final String ANDROID = "ANDROID";
    public static final String IOS = "IOS";
    public static final String PHP = "PHP";
    public static final String JAVASCRIPT = "JAVASCRIPT";
    public static final String PYTHON = "PYTHON";
    public static final String RUBY = "RUBY";


    public static final String TAG = MainActivity.class.getSimpleName();
    RecyclerView horizontalRecyclerView, verticalRecyclerView;
    SkillAdapter skillAdapter;
    JobsAdapter jobsAdapter;
    ArrayList<SkillsModel> skillsList = new ArrayList<>();
    Gson gson;
    EditText editText;
    OnResultClicked resultListener;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        gson = new Gson();

        editText = (EditText) findViewById(R.id.edit_text_id);
        horizontalRecyclerView = (RecyclerView) findViewById(R.id.horizontal_recycler_view);
        verticalRecyclerView = (RecyclerView) findViewById(R.id.vertical_recycler_view);
        horizontalRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        resultListener = this;
        generateSkill();
        generateResults(ANDROID);

        skillAdapter = new SkillAdapter(this, skillsList, this);
        horizontalRecyclerView.setAdapter(skillAdapter);

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (editable.toString().length() > 3){
                    generateResults(editable.toString());
                }
            }
        });
    }

    private void generateResults(String query) {

        HttpUrl.Builder urlBuilder = HttpUrl.parse("https://jobs.github.com/positions.json").newBuilder();
        urlBuilder.addQueryParameter("description", query);
        String url = urlBuilder.build().toString();

        OkHttpClient client = new OkHttpClient();
        final Request request = new Request.Builder().url(url).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Log.d(TAG, e.getMessage());
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
            if (response.isSuccessful()){
                String jsonstring = response.body().string();
              //  final ResultModel[] rmodel1 = gson.fromJson(jsonstring, ResultModel[].class);

                Type jasonType = new TypeToken<List<ResultModel>>(){}.getType();
                final List<ResultModel> posts = gson.fromJson(jsonstring, jasonType);

                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        jobsAdapter = new JobsAdapter(MainActivity.this, posts, MainActivity.this);
                        verticalRecyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                        verticalRecyclerView.setAdapter(jobsAdapter);
                        jobsAdapter.notifyDataSetChanged();

                    }
                });
            }
            }
        });

    }

    private void generateSkill() {
        SkillsModel skill1 = new SkillsModel();
        skill1.setSkill("ANDROID");
        skillsList.add(skill1);

        SkillsModel skill2 = new SkillsModel();
        skill2.setSkill("IOS");
        skillsList.add(skill2);

        SkillsModel skill3 = new SkillsModel();
        skill3.setSkill("PHP");
        skillsList.add(skill3);

        SkillsModel skill4 = new SkillsModel();
        skill4.setSkill("JAVASCRIPT");
        skillsList.add(skill4);

        SkillsModel skill5 = new SkillsModel();
        skill5.setSkill("PYTHON");
        skillsList.add(skill5);

        SkillsModel skill6 = new SkillsModel();
        skill6.setSkill("RUBY");
        skillsList.add(skill6);

    }


    @Override
    public void onSkillsClicked(String text) {
        generateResults(text);
        editText.setText(text);
    }

    @Override
    public void openPosition(ResultModel selectedResult) {
        if (selectedResult != null){
            String url = selectedResult.getUrl();
            Intent intent = new Intent(this, WebViewActivity.class);
            intent.putExtra(WebViewActivity.URL_EXTRA, url);
            startActivity(intent);
            }
    }
}
