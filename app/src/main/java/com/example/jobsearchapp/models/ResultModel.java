package com.example.jobsearchapp.models;

import android.os.Parcel;
import android.os.Parcelable;

public class ResultModel implements Parcelable {

    String id, type, url, company, location, title, description, company_logo;


    public ResultModel() {
    }

    protected ResultModel(Parcel in) {
        id = in.readString();
        type = in.readString();
        url = in.readString();
        company = in.readString();
        location = in.readString();
        title = in.readString();
        description = in.readString();
        company_logo = in.readString();
    }

    public static final Creator<ResultModel> CREATOR = new Creator<ResultModel>() {
        @Override
        public ResultModel createFromParcel(Parcel in) {
            return new ResultModel(in);
        }

        @Override
        public ResultModel[] newArray(int size) {
            return new ResultModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCompany_logo() {
        return company_logo;
    }

    public void setCompany_logo(String company_logo) {
        this.company_logo = company_logo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(type);
        dest.writeString(url);
        dest.writeString(company);
        dest.writeString(location);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(company_logo);
    }
}
